

class TextOperations:
    """Contain all text challenges
    """

    def reverse(self, text):
        """This is simple reverse string function.

        :param text: String
        :return: Reverse string
        """

        return text[::-1]
